FROM php:7.3-fpm

USER root

ARG SOURCES_MIRROR=""

RUN if [ ! -z "${SOURCES_MIRROR}" ]; \
        then echo "$SOURCES_MIRROR" > /etc/apt/sources.list ; \
    fi

RUN apt-get update \
    && \
        apt-get install -y git \
        bzip2 \
        libbz2-dev \
        curl \
        libcurl4-openssl-dev \
        libmcrypt-dev \
        libssl-dev \
        libxml2 \
        libxml2-dev \
        libpng-dev \
        zip \
        mariadb-client \
    && \
        apt-get clean all

RUN apt-get update && \
        apt-get install aptitude -y && \
        aptitude install libxft-dev -y \
        && sed -i 's/access.log = \/proc\/self\/fd\/2/access.log = \/proc\/self\/fd\/1/g' /usr/local/etc/php-fpm.d/docker.conf \
        && sed -i 's/;php_admin_value\[error_log\] = \/var\/log\/fpm-php.www.log/php_admin_value[error_log] = \/proc\/self\/fd\/2/g' /usr/local/etc/php-fpm.d/www.conf \
        && sed -i 's/;php_admin_flag\[log_errors\] = on/php_admin_flag\[log_errors\] = on/g' /usr/local/etc/php-fpm.d/www.conf \
        && sed -i 's/;catch_workers_output = yes/catch_workers_output = yes/g' /usr/local/etc/php-fpm.d/www.conf \
        && apt-get clean all

RUN docker-php-ext-install bz2
RUN docker-php-ext-install bcmath
RUN docker-php-ext-install calendar
RUN docker-php-ext-install curl
RUN docker-php-ext-install mysqli
RUN docker-php-ext-install pdo
RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install opcache
RUN docker-php-ext-install soap
RUN docker-php-ext-install intl
RUN docker-php-ext-install gd
RUN echo yes | pecl install xdebug

ENV PHPREDIS_VERSION 3.0.0

RUN mkdir -p /usr/src/php/ext/redis \
    && curl -L https://github.com/phpredis/phpredis/archive/$PHPREDIS_VERSION.tar.gz | \
        tar xvz -C /usr/src/php/ext/redis --strip 1 \
    && echo 'redis' >> /usr/src/php-available-exts \
    && docker-php-ext-install redis

RUN apt-get install -y wget

COPY build/config/php/php.ini /usr/local/etc/php/php.ini

